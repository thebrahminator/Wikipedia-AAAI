from gensim.models import KeyedVectors

en_model = KeyedVectors.load_word2vec_format('Datasets/wiki.en/wiki.en.vec')


words = []

for word in en_model.vocab:
    words.append(word)

#print(len(words))

print("Dimension of a word: {}".format(len(en_model[words[0]])))
'''
find_similar_to = 'car'

for similar_word in en_model.similar_by_word(find_similar_to,topn=15):
    print("Word: {0}, Similarity: {1:.2f}".format(
        similar_word[0], similar_word[1]
    ))
'''

word_add = ["cricket", "iphone", "classroom"]
word_sub = ["drone", "flight"]

for resultant_word in en_model.most_similar(
    positive=word_add, negative=word_sub,topn=20
):
    print("Word : {0} , Similarity: {1:.2f}".format(
        resultant_word[0], resultant_word[1]
    ))
