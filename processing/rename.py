#rename all files with first line of text
import os

#folder name which contains all files to b renamed
folder="bbc"


for filename in os.listdir(folder):
  with open(folder+"/"+filename) as openfile:
    firstline = openfile.readline()
    head, tail = openfile.read().split('\n', 1);
  with open(folder + "/" + filename,'w') as openfile:
    openfile.write(tail)
  firstline=firstline.replace("/", "-")
  print(firstline+" "+filename)
  os.rename(folder+"/"+filename, folder+"/"+firstline.strip()+".txt")